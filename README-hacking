* Overview

This file provides instructions on how to build and hack on the rex sources
obtained from Git repository.

For generic information about Rex and its purposes, please see the file
README.

* Prerequisites

The rex toolchain consists of the following tools:

- GNU Make <http://www.gnu.org/software/make/>
- GNU AWK <http://www.gnu.org/software/gawk/>
- GNU tar <http://www.gnu.org/software/tar/>
- Expect <http://expect.sourceforge.net>

For normal functioning of rex only the latter is required.

* Workflow

No special bootstrapping is needed, the sources are ready for
running and installing.  The main script, rex, can be run
without installing, although some defaults can be confusing.

The file GNUmakefile contains Make rules for building the
distribution.  Normally, typing

    make dist 

will do the job.

Some rules depend on GNU AWK.  If it is available under a different
name or in another directory, use the AWK variable to indicate it,
e.g.:  make AWK=gawk dist. 

* Make commands

 make dist         Create distribution tarball.
 make ChangeLog    Create ChangeLog from git log.
 make help         Displays this document.

* The install tool

All job on creating distribution tarballs and installing the package
is done by the "install" script.  See the file README for generic
instructions.  The invocation syntax is:

  install [OPTIONS] [ASGN]

The options are:  

    -n, --dry-run     do nothing, print what would have been done
    -k, --keep-going  continue working after encountering an error
    -v, --verbose     verbosely pring what is being done
    --dist            create distribution tarball and terminate
    --prefix=DIR      set installation prefix (default /usr)
    --sysconfdir=DIR  set configuration directory (/etc/rex)
    --bindir=DIR      set directory for user executables ($prefix/bin)
    --mandir=DIR      set man documentation root directory ($prefix/share/man)
    --tclpackagedir=DIR
	              directory for installing TCL packages
    --extras          install extra packages

ASGN are environment variable assignments in the form NAME=VALUE.  The
only meaningful variable is DESTDIR, which changes the topmost
directory from / to the given value.  E.g. the effect of

    ./install DESTDIR=/tmp --prefix=/usr/local

is that rex will create the necessary directory structure and install
files under /tmp/usr/local, so that the main executable will be
located in /tmp/usr/local/bin, etc.  Use this for packaging purposes.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
