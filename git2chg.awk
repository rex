# This file is part of grecs
# Copyright (C) 2007-2016 Sergey Poznyakoff
#
# Grecs is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Grecs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Grecs.  If not, see <http://www.gnu.org/licenses/>.

BEGIN {
    if (since)
	split(since,since_a,"-")
    fmt = "fmt -w 70 | sed 's/^/        /'"
    nl = 1
    if (amend) {
	line = 0
	while ((getline < amend) > 0) {
	    ++line
	    if (NF == 0 || match($1, "^#"))
		continue
	    if (match($0, "^s[[:punct:]]")) {
		delim = substr($0, RSTART+1, 1)
		text = substr($0, RSTART+2)
		rx = ""
		while ((n = index(text, delim)) != 0 \
		       && substr(text, n - 1, 1) == "\\") {
		    rx = (rx substr(text, 1, n - 2) delim)
		    text = substr(text, n+1);
		}
		if (n)
		    rx = (rx substr(text, 1, n - 1))

		text = substr(text, n + 1)
		repl = ""
		while ((n = index(text, delim)) != 0 \
		       && substr(text, n - 1, 1) == "\\") {
		    repl = (repl substr(text, 1, n - 2) delim)
		    text = substr(text, n+1);
		}
		if (n)
		    repl = (repl substr(text, 1, n - 1))

		amendrx[hash,ac] = rx
		amendrepl[hash,ac] = repl
		amendflag[hash,ac] = substr(text, n+1)
		amendc[hash] = ++ac
	    } else if (NF == 1 && length($1) == 40) {
		hash = $1
		ac = 0
	    } else {
		printf("%s:%d: malformed line\n", amend, line);
		exit(1)
	    }
	}
    }
}

function timeok(t,   a) {
    if (!since)
	return 1
    split(t,a,"-")
    if (a[1] < since_a[1])
	return 0
    if (a[1] > since_a[1])
	return 1
    if (a[2] < since_a[2])
	return 0
    if (a[2] > since_a[2])
	return 1
    return a[3] > since_a[3]
}

/^commit/ {
    commit=$2
    close(fmt)
    next
}
/^Id:/ {
    s = strftime("%F", $2)
    if (!timeok(s))
	exit
    sub(/^Id: [0-9]+ +/,"")

    tag = s "  " $0
#    if (tag == saved_tag)
#	next
#    saved_tag = tag

    if (!nl) print ""
    print tag

    details = 0
    nl = 1
    next
}
/^Subject:/ {
    sub(/^Subject: */,"")
    printf("\n%s\n\n", $0) | fmt
    close(fmt)
    next
}
/^Signed-off-by:/ { next }
/^<unknown>$/ { next }
/^ *[0-9][0-9]*\./ {
    close(fmt)
}
/^\*/ {
    close(fmt)
    if (!details) {
	if (!nl) print ""
	details = 1
    }
}
/^\(/ {
    close(fmt)
}
{   if (NF == 0) {
        if (!nl) {
	    close(fmt)
	    print ""
	    nl = 1
	}
    } else {
	if (commit in amendc) {
	    for (i = 0; i < amendc[commit]; i++) {
		if (amendflag[commit,i] == "g")
		    gsub(amendrx[commit,i], amendrepl[commit,i]);
		else
		    sub(amendrx[commit,i], amendrepl[commit,i]);
	    }
	}
	nl = 0
	print $0 | fmt
    }
}
END {
    if (append) {
	print ""
	while ((getline < append) > 0) {
	    if (match($0, /^Local *Variables:/))
		break
	    print
	}
    }
    print "\f"
    # Make sure Emacs won't recognize this line:
    print "Local", "Variables:"
    print "mode: change-log"
    print "version-control: never"
    print "buffer-read-only: t"
    print "End:"
}
