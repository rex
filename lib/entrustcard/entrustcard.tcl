# This file is part of rex - a remote execution utility
# Copyright (C) 2012, 2013, 2016 Sergey Poznyakoff
#
# Rex is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Rex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rex.  If not, see <http://www.gnu.org/licenses/>.

package provide Entrustcard 1.0
package require Tcl 8.5

namespace eval ::entrustcard {
    namespace export load decode
    namespace ensemble create
    
    # load FILE ARRAY
    #
    # Reads a card FILE into the upper-level variable VAR.
    # A card file represents an Entrust card as a two-dimensional matrix.
    # Consecutive matrix rows are listed on separate lines, each row being
    # prefixed with its number (a decimal digit from 1 to 5) followed by any
    # amount of white space.  Each row must contain exactly 10 characters.
    # Empty lines and lines starting with whitespace are ignored.  A matrix
    # definition can be followed by arbitrary text, provided that it begins
    # with a whitespace character. 
    proc load {file var} {
	upvar $var x
	set fd [open $file r]
	set lnum 0
	while {[gets $fd line] >= 0} {
	    set line [string trimright $line]
	    if {[string is space -failindex i $line] == 0 && $i > 0} {
		continue
	    }
	    if {[llength $line] == 0} {
		continue
	    }
	    incr lnum
	    if {$lnum > 5} {
		puts stderr "$file:$lnum: garbage at the end of file ignored"
		break
	    }
	    if {[scan $line "%d  %s" n s] != 2} {
		puts stderr "$file:$lnum: illegal input"
		return 1
	    }
	    if {$n != $lnum} {
		puts stderr "$file:$lnum: input line out of order"
		return 1
	    }
	    if {[string length $s] != 10} {
		puts stderr "$file:$lnum: illegal input"
		return 1
	    }
	    foreach ch [split $s {}] i {A B C D E F G H I J} {
		set x($i,$lnum) $ch
	    }
	}
	close $fd
	return 0
    }
    
    proc decode {input var} {
	upvar $var card
	set ret {}
	set a [regexp -all -inline {\[([[:alnum:]][[:alnum:]])\]} $input]
	if [llength $a] {
	    for {set i 1} {$i < [llength $a]} { incr i 2 } {
		set x [split [lindex $a $i] {}]
		set ret [lappend ret $card([lindex $x 0],[lindex $x 1])]
	    }
	    return [join $ret {}]
	}
	return -code error "not a valid challenge string"
    }
}
