# This file is part of rex - a remote execution utility
# Copyright (C) 2012, 2013, 2015, 2016 Sergey Poznyakoff
#
# Rex is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Rex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rex.  If not, see <http://www.gnu.org/licenses/>.

# For instructions and a list of configurable variables, see the description
# of startvpnc procedure below.

package provide Vpnc 1.0

package require Tcl 8.5
package require Expect 5.44.1.15
package require Entrustcard 1.0

namespace eval ::vpnc {
    namespace export checkpid start
    namespace ensemble create

    # Store the credential "what" for the connection "id" in the variable
    # "retName".  Return 1 on success, 0 on failure.
    #
    # This function always returns failure.  The user is supposed to
    # override it if the need be.
    #
    proc get_credentials {id what retName} {
#	upvar $retName ret
	return 0
    }

    variable vpnc_command "/usr/local/sbin/vpnc"
    variable pidof_command "/sbin/pidof"
    
    proc checkpid {pidfile} {
	variable cleanup_command
	variable pidof_command
	
	if ![file exists $pidfile] {
	    return 0
	}
	set fd [open $pidfile "r"]
	set pid [gets $fd]
	close $fd
	if {[catch {exec $pidof_command vpnc 2>@1} res] ||
	    [lsearch [split $res] $pid] == -1} {
	    if {[info exists cleanup_command] && 
		[file executable $cleanup_command]} {
		exec sudo $cleanup_command $pidfile
	    } else {
		exec sudo /usr/bin/rm $pidfile
	    }
	    return 0
	}
	return 1
    }

    # start ID CFGFILE PIDFILE
    #
    # Start VPN identified by ID, using configuration file CFGFILE and
    # PID file PIDFILE.
    #
    # If the VPN server requires Entrust card authentication, the variable
    # vpnc::carddir must be set to the directory keeping card files. Each
    # file must be stored in a file named by its number. See the description of
    # the procedure readcards in file entrustcard.tcl for its format.
    #
    # The current user must be able to execute VPNC binary (i.e.
    # $vpnc_command) via sudo without password. To remove stale PID files,
    # they must be able to run /usr/bin/rm on files in VPNC
    # state directory. This too requires a sudo privilege, which is normally
    # set in /etc/sudoers as follows:
    #
    #   VPNC  ALL=(ALL) NOPASSWD: /usr/bin/rm /var/run/vpnc/*.pid
    #
    # In case it is regarded as a too risky permission, set variable
    # vpnc::cleanup_command to the full pathname of a wrapper script
    # to be run instead of rm. The script should accept the full
    # pathname of a PID file as its argument and remove it if it sees fit. An
    # example vpnc-cleanup script is supplied in the rex distribution.
    #
    proc start {args} {
	variable carddir
	variable vpnc_command
	
	while {[llength $args] > 0} {
	    switch -regexp -- [lindex $args 0] {
		{^-user$} {
		    set username [lindex $args 1]
		    set args [lreplace $args [set args 0] 1]
		}
		{^-pass$} {
		    set password [lindex $args 1]
		    set args [lreplace $args [set args 0] 1]
		}
		{^--$} { break }
		{^-.*} {
		    return -code error "unknown option [lindex $args 0]"
		}
		default { break }
	    }
	}

	set id [lindex $args 0]
	set cfgfile [lindex $args 1]
	set pidfile [lindex $args 2]
	
	if [checkpid $pidfile] {
	    return
	}

	puts "Starting vpn $id"

	set password_sent 0

	spawn -ignore HUP -noecho sudo $vpnc_command --local-port 0 --pid-file $pidfile $cfgfile 

	expect {
	    "Enter a response to the grid challenge*\n" {
		if {![regexp {Enter a response to the grid challenge \[([[:alnum:]])([[:alnum:]])\] \[([[:alnum:]])([[:alnum:]])\] \[([[:alnum:]])([[:alnum:]])\] using a card with serial number ([[:digit:]]+)\.$} [string trimright $expect_out(0,string)] dummy x1 y1 x2 y2 x3 y3 n]} {
		    close
		    return -code error "$id: cannot parse reply string: $expect_out(0,string)"
		} 

		if {![info exist carddir]} {
		    return -code error "VPN \"$id\" requires entrustcard authentication (card $n), but carddir is not set"
		}

		if {[entrustcard load "$carddir/$n" card]} {
		    exit 1
		}

		set reply [entrustcard decode $expect_out(0,string) card]
		exp_continue
	    }
	    -re {username for.*:[[:space:]]*$} {
		if {[info exist username] || \
			[get_credentials $id username username]} {
		    send "$username\r"
		    exp_continue
		} else {
		    close
		    return -code error "$id: username required but not supplied"
		}
	    }
	    -re {assword for.*:[[:space:]]*$} {
		# VPNC uses getpass(3), which needs some time to open
		# /dev/tty and alter tty settings.  The code below waits
		# for one second before sending the reply.  Hopefully this
		# is enough for the things to settle.
		if {[info exist reply]} {
		    sleep 1
		    send "$reply\r"
		    unset reply
		    incr password_sent
		} elseif {$password_sent == 0} {
		    if {[info exist password] || \
			    [get_credentials $id password password]} {
			sleep 1
			send "$password\r"
			incr password_sent
		    } else {
			close
			return -code error "$id: password required but not supplied"
		    }
		} elseif {$password_sent == 1} {
		    if {[get_credentials $id password password]} {
			close
			return -code error "$id: password not accepted"
		    }
		    sleep 1
		    send "$password\r"
		    incr password_sent
		} else {
		    close
		    return -code error "$id: password not accepted"
		}
		exp_continue
	    }
	    "VPNC started*"   {
		puts -nonewline $expect_out(0,string)
	    }
	    timeout {
		close
		return -code error "$id: timed out"
	    }
	}
    }
    wait -nowait
}
